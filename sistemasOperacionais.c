/*
  Define import de bibliotecas
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
/*
  Define variaveis  e constantes global
*/
#define debug
#define tamanhoBuffer 8
pthread_mutex_t exclusao_mutua =  PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t tem_vaga        =  PTHREAD_COND_INITIALIZER;
pthread_cond_t tem_dado        =  PTHREAD_COND_INITIALIZER;

int    dados_disponiveis  = 0;
int    proximo_insercao   = 0;
int    proximo_remocao    = 0;
const  numeroThreads      = 5;

/*
  Define a strutura de dados  utilizado
*/
typedef struct dados {
	int id ;
	char frase[5];
}dados;
/*
  Define a strutura de dados  utilizado pelo buffer
*/
struct Estruturabuffer {
	 int  id;
	 char item_type;
	 int  contador;
	 int  timer; 
};
/*
	buffer global
*/

struct  Estruturabuffer buffer[6][8]; 



/*  
   funcao  principal : main
*/

int main(char *args)
{
	
   /*Prototype de functions*/
   char   letra(char *letr);
   int    buscaBuffer(char tipo) ;
   void   insere(struct Estruturabuffer  novo_dado);
   void   retiraItem(struct Estruturabuffer  *retirado_item ,struct dados  *consumidor );
   void   *produtor(struct dados *obj);
   void   *consumidor(struct dados *obj);
   int    sorteiaItem();
   int    tempoProducao();
   void   imprimirHeader();
   int    in_array(char caract,char *vetor);
   void   initBuffer(struct  Estruturabuffer **b);
   
   
   /*Inicializando variaveis locais*/
   int   i=0,
	     status=0,
	     numProdutores= 0,
	     numConsumidores= 0
   ;
   struct dados p[5],
				c[5]
   ;
   
   pthread_t  theadsC[numeroThreads] ,
              theadsP[numeroThreads] 
   ;
   
  
   
//   initBuffer(&buffer);
   /* ---------------------------------------------------------------------
	|        Entrada de dados                                             |
     ---------------------------------------------------------------------
  */
  
   /*Leitura numero de produtores  */
   printf("Digite o numero de produtores:");
   scanf("%i",&numProdutores);
   
   
   /*Leitura numero de consumidores */
   printf("Digite o numero de consumidores:");
   scanf("%i",&numConsumidores);

  /* -----------------------------------------------------------------------
	|       Tratando as Entrada de dados                                   |
     -----------------------------------------------------------------------
  */
   if(numProdutores > 5 || numConsumidores > 5)
   {
		printf("Numero de Produtores ou Consumidores Excede o Limite !");
		return 0;
   }
   
   if(numProdutores < 1  || numConsumidores < 1)
   {
		printf("Numero de Produtores ou Consumidores Deve ser maior ou Igual 1 !");
		return 0;
   }
   
 /* ------------------------------------------------------------------------
	|        Entrada de dados   2                                          |
     -----------------------------------------------------------------------
  */
   for(i=1; i<=numProdutores; i++ )
   {	   
	    p[i].id=i;
	    fflush(stdin);
		printf("Digite os tipos de itens que o produtor %i e capaz de produzir:\n",i);
	    gets(p[i].frase);
   }
   
   // limpando o buffer
   fflush(stdin); 
   
   for(i=1; i<=numConsumidores; i++ )
   {    
        c[i].id=i;
   	    fflush(stdin);
		printf("Digite os tipos de itens que o consumidor %i e capaz de consumir:\n",i);
		gets(c[i].frase);	
   }
   
   /*-------------------------------------------
      formatacao de saida                      |
     ------------------------------------------
   */
    imprimirHeader();
    
    
    
    
  /* ---------------------------------------------------------------------
	|        Tratandos os dados de Entrada  para Threads                 |
     ---------------------------------------------------------------------
  */
  

  
  

  
  
  /* ---------------------------------------------------------------------
	|         Inicializa Threads                                         |
    ----------------------------------------------------------------------
  */

   pthread_mutex_init(&exclusao_mutua,NULL);	
    	
   for(i=1; i<=numProdutores ; i++ )
   {
   	  	status=pthread_create( 
			                  &(theadsP[i]),
							  NULL,
							  (void *)  produtor, 
							  &p[i]
							  );
   }
      
   for(i=1; i<=numConsumidores; i++ )
   {
   	 
		status=pthread_create(  
		                      &(theadsP[i]),
							  NULL,
							  (void *)consumidor,
							  &c[i]
							  );
   }
   

    
  /* -----------------------------------------------------------------------
	|        Testando                                                      |
     -----------------------------------------------------------------------
  */

#ifdef debug
   while(1)
   {
   	 /* loop infinito para ver as messagens das threads segundarias,
	    senao o termino do programa iria matar as outras threads
	  */
   }
#endif
	return 1;
}

/*
 @paremetro  : ponteiro para char 
 @retorno   :  char : do caracter que deve ser utilizado pelo pelo consumidor ou produtor
*/
char letra(char *letr)
{
	char tmp='-';
	while(1)
	{ 
        int sort=sorteiaItem();
	
        if( (letr[sort]!=' '  && letr[sort]!='\0' ) && (letr[sort]=='A' || letr[sort]=='B' || letr[sort]=='C'|| letr[sort]=='D' || letr[sort]=='E' ) ) 
		{		
		  return letr[sort];	
		}			    	
	}
		
return tmp;	 
}

/*
@parameter struct buffer , int id : void

insere no buffer para o consumidor.
*/
void  insere(struct Estruturabuffer  novo_dado) 
{
	
	pthread_mutex_lock(&exclusao_mutua);
	
	while(dados_disponiveis == tamanhoBuffer) {	
		pthread_cond_wait(&tem_vaga,&exclusao_mutua);
	}
		 
    buffer[novo_dado.id][proximo_insercao] = novo_dado;
	       
    proximo_insercao = (proximo_insercao + 1) % tamanhoBuffer;
	
    ++dados_disponiveis;

    pthread_cond_signal(&tem_dado);
    pthread_mutex_unlock(&exclusao_mutua);
	
}
/*
   @paramentro : struct point
   @retorno  : void 
 */
void *produtor(struct dados *obj)
{       
     char *l,tmp_l;
	 int tempo = 0,
	     id    =(*obj).id
	 ;
     while(1)
	 {
	 	 struct Estruturabuffer novo_dado;
	 	 
         l=(*obj).frase;
	  
		 tmp_l=letra(l);
		 tempo= tempoProducao();

         novo_dado.id = id;
         novo_dado.item_type = tmp_l;
         novo_dado.timer = tempo;
         novo_dado.contador = dados_disponiveis;
         insere(novo_dado);  
           
         printf("\n\t O Produtor %d produziu o seu item ,do tipo %c (%ims) \n ",id, tmp_l , tempo);
		 sleep(1);
	  
	}
}
int  buscaBuffer(char tipo){
	int tmp_i=0, tmp_e=0;

	for(tmp_i = 1; tmp_i < 6;  tmp_i++ ) {	
		for(tmp_e = 1; tmp_e < 8;  tmp_e++ ) {
		    if( tipo == buffer[tmp_i][tmp_e].item_type ) {
				return  tmp_i ;
			}
		}	
	}	
	
    return tmp_i ;	
}
/*
@parameter struct buffer , int id : void

retira do buffer para o consumidor.
*/
void retiraItem(struct Estruturabuffer  *retirado_item , struct dados  *consumidor) 
{

	pthread_mutex_lock(&exclusao_mutua);
	while(dados_disponiveis == 0){
		pthread_cond_wait(&tem_dado,&exclusao_mutua);
	}
	int x=0;
	int idProdutor = 0 ;
	while((*consumidor).frase[x]){
	  char carac =(*consumidor).frase[x] ;		
	  idProdutor = (int) 	buscaBuffer( 
	                 carac
					 ) ;	
	  if(idProdutor!=0){
		 break;
	  }
		 
	 x++;				
	}
	
	*retirado_item  = buffer[idProdutor][proximo_remocao];
	proximo_remocao = (proximo_remocao + 1 ) % tamanhoBuffer ;
	--dados_disponiveis;
	
	pthread_cond_signal(&tem_vaga);
    pthread_mutex_unlock(&exclusao_mutua);
	
}

/*
   @paramentro :
   @retorno  : void            
 */
void *consumidor(struct dados *obj)
{
    int tempo = 0 ;
	while(1)
    {
	     struct Estruturabuffer retira;
         tempo = tempoProducao();
         retiraItem(&retira,obj);
   		 printf("\n\t O Consumidor  %d  consumiu o item %d (tipo %c)  do produtor %i (%ims) \n",(*obj).id,retira.contador,retira.item_type,retira.id,tempo);
   		 sleep(5);
    }
}

/*
  @return(int): rand timer
*/
int tempoProducao()
{
   //srand( (unsigned)time(NULL) );
	return rand() % 520000000;
}
/*
 @return(int): sorteia o item a ser produzido
*/
int sorteiaItem()
{
	return rand()%5 ;
}
/*
  @return void
  limpa tela e imprime topo
*/
void imprimirHeader() 
{
	
   system("cls");
   printf("\n--------------------------------------------------------------------------------\n");
   printf("\t\t\t\t  Executando  \n");
   printf("\n--------------------------------------------------------------------------------\n");
}

int in_array(char caract,char *vetor)
{
	 int contador = 0;
	 int flag = 0;
     while(vetor[contador]){
        if(vetor[contador] == caract) { 
          	flag++ ;
		}
	    contador++;	
	 }	
	return flag ;
}

void initBuffer(struct  Estruturabuffer **b)
{
    int tmp_i, tmp_e;

	for(tmp_i = 1; tmp_i < 6;  tmp_i++ ) {	
		for(tmp_e = 1; tmp_e < 8;  tmp_e++ ) {
			struct  Estruturabuffer tmp;
			b[tmp_i][tmp_e] =  tmp ;
		}	
	}	
}

